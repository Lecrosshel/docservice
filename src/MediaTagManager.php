<?php

namespace Lecrosshel;

class MediaTagManager
{
    /**
     * @var array
     */
    private $mediaTags = [
        "<<!image!>>",
        "<<!image:remote!>>",
        "<<!link!>>",
        "<<!space!>>",
    ];

    /**
     * @param string|null $value
     * @return \stdClass|bool
     */
    public function getTagContent(string $value = null)
    {
        if (!$value) {
            return false;
        }

        $tagFound = $this->searchTags($value);

        if (!$tagFound) {
            return false;
        }

        $cleanValue = trim(str_replace($tagFound, '', $value));
        $tagContent = new \stdClass();
        $tagContent->tag = $tagFound;
        $tagContent->value = $cleanValue;

        return $tagContent;
    }

    /**
     * @param array $values
     * @return array
     */
    public function getMultipleTagsContent(array $values)
    {
        $data = [];
        $i = 0;

        foreach ($values as $value) {
            $item = $this->getTagContent($value);

            if ($item) {
                $data[$i] = $item;
                $i++;
            }
        }

        return $data;
    }

    /**
     * @param string $value
     * @return bool|mixed
     */
    public function searchTags(string $value)
    {
        foreach ($this->mediaTags as $mediaTag) {
            preg_match('/' . $mediaTag . '\z/i', $value, $match);

            if ($match) {
                return $match[0];
            }
        }

        return false;
    }
}