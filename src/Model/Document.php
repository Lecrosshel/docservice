<?php

namespace Lecrosshel\Model;


abstract class Document
{
    /**
     * @var string
     */
    protected $timestamp;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var mixed|null
     */
    protected $data;

    /**
     * Document constructor.
     */
    public function __construct()
    {
        $this->timestamp = (string)date('Y-m-d h:i:s');
        $this->title = "document";
        $this->data = null;
    }

    /**
     * @return string|null
     */
    public function getTimestamp(): ?string
    {
        return $this->timestamp;
    }

    /**
     * @param string $timestamp
     * @return Document
     */
    public function setTimestamp(string $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Document
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    abstract function getData();

    abstract function setData($data);
}