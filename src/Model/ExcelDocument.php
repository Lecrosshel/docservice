<?php

namespace Lecrosshel\Model;


class ExcelDocument extends Document
{
    /**
     * @var integer
     */
    private $offset;

    /**
     * @var array
     */
    private $styles;

    /**
     * @var integer
     */
    private $imagesHeight;

    /**
     * @var integer
     */
    private $imagesWidth;

    /**
     * @var array
     */
    private $cellsTitleStyle;

    /**
     * ExcelDocument constructor.
     */
    public function __construct()
    {
        $this->offset = 1;
        $this->imagesHeight = 65;
        $this->imagesWidth = 65;
        $this->cellsTitleStyle = [];

        parent::__construct();
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return ExcelDocument
     */
    public function setData($data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     * @return ExcelDocument
     */
    public function setOffset(int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * @return array
     */
    public function getStyles(): ?array
    {
        return $this->styles;
    }

    /**
     * @param array $styles
     * @return ExcelDocument
     */
    public function setStyles(array $styles): self
    {
        $this->styles = $styles;

        return $this;
    }

    /**
     * @return int
     */
    public function getImagesHeight(): int
    {
        return $this->imagesHeight;
    }

    /**
     * @param int $imagesHeight
     * @return ExcelDocument
     */
    public function setImagesHeight(int $imagesHeight): self
    {
        $this->imagesHeight = $imagesHeight;

        return $this;
    }

    /**
     * @return int
     */
    public function getImagesWidth(): int
    {
        return $this->imagesWidth;
    }

    /**
     * @param int $imagesWidth
     * @return ExcelDocument
     */
    public function setImagesWidth(int $imagesWidth): self
    {
        $this->imagesWidth = $imagesWidth;

        return $this;
    }

    /**
     * @return array
     */
    public function getCellsTitleStyle(): array
    {
        return $this->cellsTitleStyle;
    }

    /**
     * @param array $cellsTitleStyle
     * @return ExcelDocument
     */
    public function setCellsTitleStyle(array $cellsTitleStyle): self
    {
        $this->cellsTitleStyle = $cellsTitleStyle;

        return $this;
    }
}