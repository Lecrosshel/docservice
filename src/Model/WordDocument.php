<?php

namespace Lecrosshel\Model;

class WordDocument extends Document
{
    /**
     * @var array
     */
    private $mainStyle;

    /**
     * @var array
     */
    private $titleFontStyle;

    /**
     * @var int
     */
    private $textBreakSize;

    /**
     * @var string
     */
    private $fooText;

    /**
     * @var array
     */
    private $fooTextStyle;

    /**
     * @var array
     */
    private $categoryFontStyle;

    /**
     * @var array
     */
    private $subcategoryFontStyle;

    /**
     * @var array
     */
    private $bodyFontStyle;

    /**
     * @var int
     */
    private $colsNum;

    /**
     * @var array
     */
    private $cols;

    /**
     * @var int
     */
    private $itemSpacing;

    /**
     * @var bool
     */
    private $escapeSpecialChars;

    /**
     * WordDocument constructor.
     */
    public function __construct()
    {
        $this->mainStyle = [];
        $this->titleFontStyle = [];
        $this->textBreakSize = 0;
        $this->fooText = '';
        $this->fooTextStyle = [];
        $this->categoryFontStyle = [];
        $this->subcategoryFontStyle = [];
        $this->bodyFontStyle = [];
        $this->colsNum = 1;
        $this->cols = [];
        $this->itemSpacing = 0;
        $this->escapeSpecialChars = true;

        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param $data
     * @return WordDocument
     */
    public function setData($data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return array
     */
    public function getMainStyle(): array
    {
        return $this->mainStyle;
    }

    /**
     * @param array $mainStyle
     * @return WordDocument
     */
    public function setMainStyle(array $mainStyle): self
    {
        $this->mainStyle = $mainStyle;

        return $this;
    }

    /**
     * @return array
     */
    public function getTitleFontStyle(): array
    {
        return $this->titleFontStyle;
    }

    /**
     * @param array $titleFontStyle
     * @return WordDocument
     */
    public function setTitleFontStyle(array $titleFontStyle): self
    {
        $this->titleFontStyle = $titleFontStyle;

        return $this;
    }

    /**
     * @return int
     */
    public function getTextBreakSize(): int
    {
        return $this->textBreakSize;
    }

    /**
     * @param int $textBreakSize
     * @return WordDocument
     */
    public function setTextBreakSize(int $textBreakSize): self
    {
        $this->textBreakSize = $textBreakSize;

        return $this;
    }

    /**
     * @return string
     */
    public function getFooText(): string
    {
        return $this->fooText;
    }

    /**
     * @param string $fooText
     * @return WordDocument
     */
    public function setFooText(string $fooText): self
    {
        $this->fooText = $fooText;

        return $this;
    }

    /**
     * @return array
     */
    public function getFooTextStyle(): array
    {
        return $this->fooTextStyle;
    }

    /**
     * @param array $fooTextStyle
     * @return WordDocument
     */
    public function setFooTextStyle(array $fooTextStyle): self
    {
        $this->fooTextStyle = $fooTextStyle;

        return $this;
    }

    /**
     * @return array
     */
    public function getCategoryFontStyle(): array
    {
        return $this->categoryFontStyle;
    }

    /**
     * @param array $categoryFontStyle
     * @return WordDocument
     */
    public function setCategoryFontStyle(array $categoryFontStyle): self
    {
        $this->categoryFontStyle = $categoryFontStyle;

        return $this;
    }

    /**
     * @return array
     */
    public function getSubcategoryFontStyle(): array
    {
        return $this->subcategoryFontStyle;
    }

    /**
     * @param array $subcategoryFontStyle
     * @return WordDocument
     */
    public function setSubcategoryFontStyle(array $subcategoryFontStyle): self
    {
        $this->subcategoryFontStyle = $subcategoryFontStyle;

        return $this;
    }

    /**
     * @return array
     */
    public function getBodyFontStyle(): array
    {
        return $this->bodyFontStyle;
    }

    /**
     * @param array $bodyFontStyle
     * @return WordDocument
     */
    public function setBodyFontStyle(array $bodyFontStyle): self
    {
        $this->bodyFontStyle = $bodyFontStyle;

        return $this;
    }

    /**
     * @return int
     */
    public function getColsNum(): int
    {
        return $this->colsNum;
    }

    /**
     * @param int $colsNum
     * @return WordDocument
     */
    public function setColsNum(int $colsNum): self
    {
        $this->colsNum = $colsNum;

        return $this;
    }

    /**
     * @return array
     */
    public function getCols(): array
    {
        return $this->cols;
    }

    /**
     * @param array $cols
     * @return WordDocument
     */
    public function setCols(array $cols): self
    {
        $this->cols = $cols;

        return $this;
    }

    /**
     * @return int
     */
    public function getItemSpacing(): int
    {
        return $this->itemSpacing;
    }

    /**
     * @param int $itemSpacing
     * @return WordDocument
     */
    public function setItemSpacing(int $itemSpacing): self
    {
        $this->itemSpacing = $itemSpacing;

        return $this;
    }

    /**
     * @return bool
     */
    public function escapeSpecialChars()
    {
        return $this->escapeSpecialChars;
    }

    /**
     * @param bool $escapeSpecialChars
     * @return WordDocument
     */
    public function setEscapeSpecialChars(bool $escapeSpecialChars): self
    {
        $this->escapeSpecialChars = $escapeSpecialChars;

        return $this;
    }
}