<?php

namespace Lecrosshel;


interface ConstructorInterface
{
    public function build();
}