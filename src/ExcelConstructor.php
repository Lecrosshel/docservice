<?php
namespace Lecrosshel;

use Lecrosshel\MediaTagManager AS TagManager;
use GuzzleHttp\Client AS Guzzle;
use Lecrosshel\Model\ExcelDocument;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls AS XLSWriter;

class ExcelConstructor implements ConstructorInterface
{
    private const MAX_COLUMNS_NUM = 50;

    private const DEFAULT_FONT_SIZE = 10;

    private const IMAGE_EXCEL_OFFSET = 10;

    private const DEFAULT_IMAGE_SIZE = 65;

    private $tmpFiles = [];

    /**
     * @var array
     */
    private $excelColsMap = [
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "G",
        "H",
        "I",
        "J",
        "K",
        "L",
        "M",
        "N",
        "O",
        "P",
        "Q",
        "R",
        "S",
        "T",
        "U",
        "V",
        "W",
        "X",
        "Y",
        "Z",
    ];

    private const EXCEL_KEY_CORRECTION_VALUE = 1;

    /**
     * @var array
     */
    private $excelParentMappingDecrements = [0, 0, 2, 3, 4, 5, 5, 6, 7, 8];

    /**
     * @var array
     */
    private $excelSecondaryMappingDecrements = [0, 0, 26, 26, 26, 26, 52, 52, 52];

    /**
     * @var ExcelDocument
     */
    private $document;

    /**
     * ExcelConstructor constructor.
     * @param ExcelDocument $document
     */
    public function __construct(ExcelDocument $document)
    {
        $this->document = $document;
    }


    /**
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function build()
    {
        $data = $this->document->getData();
        $tagManager = new TagManager();
        $date = $this->document->getTimestamp();
        $docTitle = $this->document->getTitle() . '-' . $date . '.xls';
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(self::DEFAULT_FONT_SIZE);
        $spreadsheet->getDefaultStyle()->getAlignment()->setVertical('top');
        $sheet = $spreadsheet->getActiveSheet();
        $colNum = $this->document->getOffset();
        $fileCnt = 0;
        $styles = $this->document->getStyles();

        if ($styles) {
            $this->addStylesToList($sheet);
        }

        foreach ($data as $values) {
            foreach ($values as $k => $value) {
                if ($k > self::MAX_COLUMNS_NUM) {
                    $sheet->setCellValue('A1', "Error: Too many columns. Maximum columns number is " . self::MAX_COLUMNS_NUM . "given " . count($values));
                    break;
                }

                if ($k && !array_key_exists($k, $this->excelColsMap)) {
                    $cellId = $this->getExcelCellId($k);
                } else {
                    $cellId = $this->excelColsMap[$k];
                }

                if ($colNum == 1) {
                    $lastCellId = $this->getExcelCellId(count($values) - 1);
                    $spreadsheet->getActiveSheet()->getStyle('A1:' . $lastCellId . '1')->applyFromArray($this->document->getCellsTitleStyle());
                }

                $hasMedia = $tagManager->getTagContent($value);

                if ($hasMedia) {
                    switch ($hasMedia->tag) {
                        case '<<!image:remote!>>':
                            $sheet->getRowDimension($colNum)->setRowHeight($this->document->getImagesHeight() + 10);
                            $this->addRemoteImageToExcel($fileCnt, $cellId, $colNum, $hasMedia->value, $sheet);
                            $fileCnt++;
                            break;
                        case '<<!image!>>':
                            $sheet->getRowDimension($colNum)->setRowHeight($this->document->getImagesHeight() + 10);
                            $this->addLocalImageToExcel($cellId, $colNum, $hasMedia->value, $sheet);
                            break;
                        case '<<!link!>>':
                            $sheet->getCell($cellId . $colNum)
                                ->getHyperlink()
                                ->setUrl($hasMedia->value)
                                ->setTooltip('Перейти к фото');
                            $sheet->setCellValue($cellId . $colNum, 'Перейти к фото');
                            break;
                        default:
                            break;
                    }
                    continue;
                }

                $spreadsheet->getActiveSheet()->getColumnDimension($cellId)->setAutoSize(true);
                $sheet->setCellValue($cellId . $colNum, $value);
            }

            $colNum++;
        }

        $writer = new XLSWriter($spreadsheet);
        $writer->save($docTitle);
        $this->removeTempFiles();
        return $docTitle;
    }

    /**
     * @param int $intKey
     * @return string
     */
    private function getExcelCellId(int $intKey): string
    {
        $cellId = "";
        $strKey = (string)$intKey;
        $keyLength = strlen($strKey);

        if ($intKey < count($this->excelColsMap)) {
            return $this->excelColsMap[$intKey];
        }

        for ($c = 0; $c < $keyLength; $c++) {
            if (!$c) {
                $cellId .= $this->excelColsMap[((int)$strKey[$c] - $this->excelParentMappingDecrements[$strKey[$c]])];
                continue;
            }

            $colsDecrement = $this->excelSecondaryMappingDecrements[$strKey[0]];

            if ($intKey < $colsDecrement) {
                $colsDecrement = $this->excelSecondaryMappingDecrements[(int)$strKey[0] - self::EXCEL_KEY_CORRECTION_VALUE];
            }

            $cellId .= $this->excelColsMap[($intKey - $colsDecrement)];
        }

        return $cellId;
    }

    /**
     * @param int $fileCnt
     * @param string $colId
     * @param int $colNum
     * @param string $url
     * @param Worksheet $sheet
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function addRemoteImageToExcel(int $fileCnt, string $colId, int $colNum, string $url, Worksheet $sheet)
    {
        $guzzle = new Guzzle(['http_errors' => false]);
        $request = $guzzle->get($url);

        if ($request->getStatusCode() == 200) {
            $dir = sys_get_temp_dir();
            $imgPath = $dir . '/temp' . $fileCnt . '.jpg';
            $isFile = file_put_contents($imgPath, $request->getBody()->getContents());

            if (!$isFile) {
                $sheet->setCellValue($colId . $colNum, 'File not found');
            }

            $drawing = new Drawing();
            $drawing->setPath($imgPath);
            $drawing->setHeight($this->document->getImagesHeight() ?? self::DEFAULT_IMAGE_SIZE);
            $drawing->setWidth($this->document->getImagesWidth() ?? self::DEFAULT_IMAGE_SIZE);
            $drawing->setOffsetX($this->document->getImagesWidth() ?? self::IMAGE_EXCEL_OFFSET);
            $drawing->setOffsetY(self::IMAGE_EXCEL_OFFSET);
            $drawing->setCoordinates($colId . $colNum);
            $drawing->setWorksheet($sheet);
            array_push($this->tmpFiles, $imgPath);
        }
    }

    /**
     * @return bool
     */
    private function removeTempFiles()
    {
        foreach ($this->tmpFiles as $file) {
            if (file_exists($file)) {
                $isDeleted = unlink($file);

                if (!$isDeleted) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param string $colId
     * @param int $colNum
     * @param string $url
     * @param Worksheet $worksheet
     */
    private function addLocalImageToExcel(string $colId, int $colNum, string $url, Worksheet $worksheet)
    {
    }

    /**
     * @param Worksheet $sheet
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function addStylesToList(Worksheet $sheet)
    {
        foreach ($this->document->getStyles() as $k => $style) {
            $sheet->getStyle($k)->applyFromArray($style);
        }
    }

}