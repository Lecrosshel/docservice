<?php

namespace Lecrosshel;

use PhpOffice\PhpWord\Element\Section;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory AS Writer;
use Lecrosshel\Model\WordDocument;
use PhpOffice\PhpWord\Settings;

class WordConstructor implements ConstructorInterface
{
    private const DEFAULT_WORD_COLUMN_WIDTH = 7000;

    private const BREAK_POINT = 10;

    private const DEFAULT_FONT_SIZE = 10;

    /**
     * @var WordDocument
     */
    private $document;

    /**
     * ExcelConstructor constructor.
     * @param WordDocument $document
     */
    public function __construct(WordDocument $document)
    {
        $this->document = $document;
    }

    /**
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function buildSimpleWord()
    {
        $phpWord = $this->buildStdWordObj();
        $phpWord->addTitle($this->document->getTitle());
        $section = $phpWord->addSection($this->document->getMainStyle());
        $section->addText($this->document->getTitle(), $this->document->getTitleFontStyle());
        $section->addText($this->document->getData(), $this->document->getBodyFontStyle());

        $this->saveWordDoc($this->document->getTitle(), $phpWord);
    }

    /**
     * @return string
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function build()
    {
        Settings::setOutputEscapingEnabled($this->document->escapeSpecialChars());
        $date = $this->document->getTimestamp();
        $docTitle = $this->document->getTitle() . '-' . $date . '.docx';
        $phpWord = $this->buildStdWordObj();
        $section = $phpWord->addSection(['breakType' => 'continuous', 'colsNum' => 1]);
        $phpWord->addParagraphStyle('titlesStyle', ['align' => 'center']);
        $section->addText($this->document->getTitle(), $this->document->getTitleFontStyle(), 'titlesStyle');
        $section->addTextBreak($this->document->getTextBreakSize());
        $styleTable = ['borderSize' => 0, 'borderColor' => 'ffffff', 'marginBottom' => 5];
        $phpWord->addTableStyle('style', $styleTable);

        if ($this->document->getFooText()) {
            $footer = $section->addFooter();
            $footer->addText($this->document->getFooText(), $this->document->getFooTextStyle(), ['align' => 'right']);
        }

        $data = $this->document->getData();

        foreach ($data as $k => $category) {
            $section->addText($k, $this->document->getCategoryFontStyle(), 'titlesStyle');
            $section->addTextBreak($this->document->getTextBreakSize());

            if (!is_array($category)) {
                $this->buildWordItems($data, $section);
            } else {
                $this->buildWordSubCategories($category, $section);
            }
        }

        $this->saveWordDoc($docTitle, $phpWord);

        return $docTitle;
    }

    /**
     * @param array $categories
     * @param Section $section
     * @param int $breakCnt
     * @return bool
     */
    private function buildWordSubCategories(array $categories, Section $section, int $breakCnt = 0)
    {
        foreach ($categories as $k => $category) {
            if (!is_array($category)) {
                $this->buildWordItems($categories, $section);
                break;
            }

            $section->addText($k, $this->document->getSubcategoryFontStyle());
            $section->addTextBreak(1);
            ++$breakCnt;

            if ($breakCnt == self::BREAK_POINT) {
                $section->addText('Break point achieved. Too many categories.');
                return false;
            }

            $this->buildWordSubCategories($category, $section, $breakCnt);
        }

        return true;
    }

    /**
     * @param array $items
     * @param Section $section
     */
    private function buildWordItems(array $items, Section $section)
    {
        $table = $section->addTable('style');

        foreach ($items as $item) {
            $table->addRow();

            for ($i = 0; $i < $this->document->getColsNum(); $i++) {
                $currentCol = "col{$i}";
                $table->addCell($this->document->getCols()[$i]["width"])->addText($item->$currentCol ?? '', $this->document->getCols()[$i]["fontStyle"] ?? []);
            }

            $row = $table->addRow()->addCell(self::DEFAULT_WORD_COLUMN_WIDTH);
            $row->getStyle()->setBorderBottomSize($this->document->getItemSpacing());
            $row->getStyle()->setBorderBottomColor('ffffff');
            $row->addText($item->body ?? '');
        }

        $section->addTextBreak($this->document->getTextBreakSize());
    }

    /**
     * @return PhpWord
     */
    private function buildStdWordObj()
    {
        $phpWord = new PhpWord();
        $phpWord->setDefaultFontName('Arial');
        $phpWord->setDefaultFontSize(self::DEFAULT_FONT_SIZE);
        $phpWord->addTitle($this->document->getTitle());

        return $phpWord;
    }

    /**
     * @param string $title
     * @param PhpWord $phpWord
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    private function saveWordDoc(string $title, PhpWord $phpWord)
    {
        $writer = Writer::createWriter($phpWord, 'Word2007');
        $writer->save('./' . $title);
    }

}