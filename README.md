# DocService
Installation

````
composer require lecrosshel/docservice
```` 

General usage

````php
//Excel document creating
$data = [
            [
                "simple_text",
                "remote_image_inserting",
                "link_inserting",
            ]
            [
                "first",
                "http://images.com/remote_image.jpg <<!image:remote!>>",
                "http://link.com <<!link!>>",
            ]
        ];
$excelDocument = new ExcelDocument();
$excelDocument->setTitle("new_document");
$excelDocument->setData($data);
$excelConstructor = new ExcelConstructor($excelDocument);
$document = $excelDocument->build();

//Word document creating

//With categories
$wordItem = new \stdClass()
$wordItem->col1 = "some text";
$data = [
          "category" => [
            "subcategory_optional" => [
              $wordItem
            ]
          ]
        ];

//Simple text
$data = "text sample";
$wordDocument = new WordDocument();
$wordDocument->setTitle("new_document");
$wordDocument->setData($data);
$wordConstructor = new WordConstructor($wordDocument);
//std with cats
$document = $wordConstructor->build();
//simple
$document = $wordConstructor->buildSimpleWord();

````

Stylization

````php
//Word stylization
$wordDocument = new WordDocument();
$wordDocument->setTitleFontStyle(["size" => 24, "bold" => true]);
$wordDocument->setCategoryFontStyle(["size" => 15, "color" => "49423d", "bold" => true]);
$wordDocument->setSubcategoryFontStyle(["size" => 16, "bold" => true]);

//Table stylization
$wordDocument->setCols([
            [
                "fontStyle" => ["size" => 10, "bold" => true],
                "width" => 7000,
            ],
            [
                "fontStyle" => [],
                "width" => 1000,
            ],
            [
                "fontStyle" => [],
                "width" => 1000,
            ],
        ]);
$wordDocument->setColsNum(3);
$wordDocument->setItemSpacing(100);
$wordDocument->setTextBreakSize(2);

//Use this option to add footer text
$wordDocument->setFooText("Created in *");
$wordDocument->setFooTextStyle(['color' => 'bbbbbb']);

//Excel stylization
$excelDocument = new ExcelDocument();
$excelDocument->setCellsTitleStyle([
            'font' => [
                'name' => 'Arial',
                'size' => '10',
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ]);
//Multiple cells stylization
private const ORDER_TABLE_HEADER_POSITION = 8;

$styles = [];
$styles["B3"] = [
                'font' => [
                    'bold' => true,
                ],
            ];
            $styles["B7"] = [
                'font' => [
                    'bold' => true,
                ],
            ];
            $rowsNum = ($itemNum) + $document->getOffset();

            for ($i = self::ORDER_TABLE_HEADER_POSITION; $i <= $rowsNum; $i++) {
                $styles["A{$i}:F{$i}"] = [
                    'alignment' => [
                        'horizontal' => "center",
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => "medium",
                        ],
                    ],
                ];

                if ($i == self::ORDER_TABLE_HEADER_POSITION) {
                    $styles["A{$i}:F{$i}"]['font'] = ['bold' => true];
                }
            }
````




